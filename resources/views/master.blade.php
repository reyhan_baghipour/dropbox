<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/app.css">
    <title>One Web Side</title>
</head>
<body class="resize">
    <div class="w-full h-auto flex justify-between items-center">
    <div class="header bg-pink-800 w-3/5">
        <div class="iconleft">
            <i class="fab fa-dropbox text-blue-300 ml-4 mt-2 text-lg lg:text-2xl lg:ml-10 lg:mt-5"></i>
            <span class="hidden lg:inline-block Nunito text-white text-2xl">Dropbox</span>
        </div>
    </div>
    <div class="header bg-pink-800 w-2/5 flex justify-end lg:bg-white">
        <div class="right flex flex-row ml-4 mt-4">
            <div class="sign-in text-white text-sm Nunito mr-4 lg:text-black lg:text-base">
                <a href="#">Sign in</a>
            </div>
            <div class="hidden lg:text-base lg:text-black lg:inline-block Nunito lg:mr-10"><a href="#">Download</a></div>
            <input class="checkbox hidden" type="checkbox" id="checkbox">
            <div class="menu w-4 h-4 bg-transparent mr-5 lg:hidden">
                <label for="checkbox" class="flex flex-col justify-around w-full h-full cursor-pointer">
                    <div class="line1 bg-white w-full h-0.5 origin-right"></div>
                    <div class="line2 bg-white w-full h-0.5 origin-right"></div>
                    <div class="line3 bg-white w-full h-0.5 origin-right"></div>
                </label>
            </div>
        </div>
    </div>
    </div>
    <div class="onepart flex flex-row bg-pink-800 w-full h-auto">
        <div class="w-full lg:w-3/5">
            <p class="Nunito text-3xl pr-72 sm:pr-72 sm:text-4xl md:pr-80 md:text-5xl lg:ml-20 lg:mr-0 lg:pr-5 text-blue-300 ml-4 pt-6 lg:pt-20">
                Put Your Creative Enerrgy To Work, With Drobox
            </p>
            <div class="matn text-white text-justify text-sm ml-4 mr-40 mt-4 sm-text-base md:text-lg lg:ml-20 lg:mt-14">
                At Dropbox we believe creative energy is a precious resource and the world needs it now more than ever. It’s not about working more, it’s about focusing on the work that truly matters, Carolyn Feinstein, Chief Marketing Officer at Dropbox.
            </div>
        </div>
        <div class="signupform hidden lg:w-2/5 lg:bg-white lg:inline-block">
            <h1 class="text-black Nunito mt-10 font-extrabold text-lg ml-20">Sign Up</h1>
            <p class="text-blue-700 Nunito text-xs mb-10 ml-20">Enter Your Information</p>
            <div class="input flex flex-col justify-center items-center">
                <input type="text" placeholder="First Name" class="w-60 mb-3 pl-2 rounded-lg border-2 hover:border-black hover:border-2xl focus:outline-none">
                <input type="text" placeholder="Last Name" class="w-60 mb-3 pl-2 rounded-lg border-2 hover:border-black hover:border-2xl focus:outline-none">
                <input type="email" placeholder="Email"class="w-60 mb-3 pl-2 rounded-lg border-2 hover:border-black hover:border-2xl focus:outline-none" >
                <input type="password" placeholder="Password" class="w-60 mb-3 pl-2 rounded-lg border-2 hover:border-black hover:border-2xl focus:outline-none">
            </div>
            <div class="checkbox ml-20 pl-1">
                <input type="checkbox" class="checkbox" id="checkbox">
                <label for="checkbox">I agree to the <span class="information text-blue-700 cursor-pointer">terms and conditions.</span></label>
            </div>
            <div class="buttonenter flex flex-col items-center">
                <button type="button" class="signup w-60 h-8 rounded-md px-2 py-1 mt-3 text-sm Nunito bg-blue-700 border-blue-700 border hover:border-blue-300 hover:text-white hover:bg-blue-300 focus:outline-none">Sign Up</button>
                <button type="button" class="signup w-60 h-8 rounded-md px-2 py-1 mt-3 mb-5 text-sm Nunito bg-white border-blue-300 border hover:border-blue-300 hover:text-white hover:bg-blue-300 focus:outline-none">Sign Up With Google</button>
            </div>
        </div>
    </div>
    <div class="twopart bg-pink-800 lg:hidden">
        <button type="button" class="signup lg:hidden w-36 h-8 rounded-md px-2 py-1 mt-5 mb-5 ml-4 text-base Nunito bg-white border-white border hover:border-blue-300 hover:text-white hover:bg-blue-300 focus:outline-none">Sign-up for free</button>
    </div>
    <div class="signupf bg-pink-800 hidden">
        <div class="input ml-4 flex flex-col">
            <h1 class="text-white Nunito mb-4">Sign Up</h1>
            <input type="text" placeholder="First Name" class="w-60 mb-3 pl-2 rounded-lg border hover:border-black hover:border-2xl focus:outline-none">
            <input type="text" placeholder="Last Name" class="w-60 mb-3 pl-2 rounded-lg border hover:border-black hover:border-2xl focus:outline-none">
            <input type="email" placeholder="Email"class="w-60 mb-3 pl-2 rounded-lg border hover:border-black hover:border-2xl focus:outline-none" >
            <input type="password" placeholder="Password" class="w-60 mb-3 pl-2 rounded-lg border hover:border-black hover:border-2xl focus:outline-none">
        </div>
        <div class="checkbox ml-4">
            <input type="checkbox" class="checkbox" id="checkbox">
            <label for="checkbox">I agree to the <span class="information text-blue-700 cursor-pointer">terms and conditions.</span></label>
        </div>
        <div class="buttonenter flex flex-col">
            <button type="button" class="signup w-60 h-8 rounded-md px-2 py-1 mt-3 ml-4 text-sm Nunito bg-blue-700 border-blue-700 border hover:border-blue-300 hover:text-white hover:bg-blue-300 focus:outline-none">Sign Up</button>
            <button type="button" class="signup w-60 h-8 rounded-md px-2 py-1 mt-3 mb-5 ml-4 text-sm Nunito bg-white border-white border hover:border-blue-300 hover:text-white hover:bg-blue-300 focus:outline-none">Sign Up With Google</button>
        </div>
    </div>
    <div class="hidden w-full h-auto lg:flex lg:flex-row">
        <div class="lg:w-3/5 bg-pink-800 lg:block lg:pb-20 lg:pl-20 text-blue-300 text-2xl">
            <a href="#down"><i class="fa fa-arrow-down"></i></a>
        </div>
        <div class="lg:w-2/5 bg-blue-300"></div>
    </div>
    <div class="gallaryimage w-full h-auto lg:hidden">
        <div class="image1 bg-pink-800 pt-10">
            <p class="text-white text-xs md:text-sm text-right mb-2 mr-6 Nunito">put your creative energy to work</p>
            <img src="../css/pic/part1.png" alt="circle" class="w-full h-full px-5">
        </div>
        <div class="image2 bg-blue-300">
            <img src="../css/pic/part2.png" alt="circle" class="w-full h-full px-5">
            <p class="text-black text-xs md:text-sm text-left mt-2 ml-6 pb-10 Nunito">put your creative energy to work</p>
        </div>
        <div class="textlast bg-blue-300 hidden invisible md:visible md:block ">
            <p class="text-pink-800 ml-4 Nunito text-xl mb-3">How Dropbox is helping put your creative energy to work</p>
            <p class="text-black ml-4 pb-20 text-lg mr-8 text-justify">Unshackling the obstructions of the modern working life keeps us honest and focused, it lets ideas flow. Ideas that are going to solve business problems, galvanise and energise teams. Ideas that will lead to the meaningful work that builds a better business.</p>
        </div>
    </div> 
    <div id="down" class="hidden lg:w-full lg:h-full lg:flex lg:flex-row">
        <div class="bg-pink-800 w-3/5 pt-20 flex flex-row">
            <div class="left w-1/2 h-full">
                <p class="text-lg px-12 pt-16 text-blue-300 Nunito">How Dropbox is helping put your creative energy to work</p>
                <p class="text-base px-12 pt-5 text-white Nunito">Instead of talking about "productivity" and "doing more" we want to help people get to their best work. So, from all of us at Dropbox, here's to you, and to the power of creative energy.</p>
            </div>
            <div class="right flex flex-col w-1/2 h-full">
                <img src="../css/pic/part3.png" alt="circle" class="w-full h-full">
                <p class="Nunito text-white text-sm pb-4 pl-4 transform origin-left -rotate-90">put your creative energy to work</p>
            </div>
        </div>
        <div class="bg-blue-300 w-2/5 pt-20 flex flex-col">
            <div class="w-3/5 h-full flex flex-col justify-center">
                <img src="../css/pic/part4.png" alt="circle" class="w-full h-full">
                <p class="Nunito text-white text-sm pb-5 pl-4 transform origin-right rotate-90">put your creative energy to work</p>
            </div>
        </div>
    </div> 
    <script>
        let form=document.querySelector(".signupf");
        let buttonsignup=document.querySelector(".twopart");
        let screen=document.querySelector(".resize");
        let isshow=false;
        buttonsignup.addEventListener("click",function(){
            isshow =! isshow;
            show();
        })
        function show(){
            if(isshow){
                form.classList.remove("hidden")
            }else{
                form.classList.add("hidden")

            }
        }
        window.addEventListener("resize", () => {
            isshow = false;
            show();
        })
    </script>
</body>
</html>